<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\CheckerComposite\Test;

use Interactiv4\CheckerComposite\CheckerComposite;
use Interactiv4\Contracts\Checker\Api\CheckerInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckerCompositeTest.
 *
 * @internal
 */
class CheckerCompositeTest extends TestCase
{
    /**
     * Test class exists and is an instance of CheckerInterface.
     */
    public function testInstanceOf(): void
    {
        $checker = new CheckerComposite();
        static::assertInstanceOf(CheckerInterface::class, $checker);
    }

    /**
     * Test default return value without arguments is true.
     */
    public function testDefaultReturnValueIsTrue(): void
    {
        $checker = new CheckerComposite();
        static::assertTrue($checker->check());
    }

    /**
     * Test default return value in constructor.
     *
     * @param string $strategy
     * @param bool   $defaultValue
     * @param bool   $expectedValue
     * @dataProvider defaultReturnValueInConstructorDataProvider
     */
    public function testDefaultReturnValueInConstructor(
        string $strategy,
        bool $defaultValue,
        bool $expectedValue
    ): void {
        $checker = new CheckerComposite([], $strategy, $defaultValue);
        static::assertSame($expectedValue, $checker->check());
    }

    /**
     * @return array
     */
    public function defaultReturnValueInConstructorDataProvider(): array
    {
        return [
            [CheckerComposite::STRATEGY_ALL, false, false],
            [CheckerComposite::STRATEGY_ALL, true, true],
            [CheckerComposite::STRATEGY_ANY, false, false],
            [CheckerComposite::STRATEGY_ANY, true, true],
        ];
    }

    /**
     * Test eager return depending on strategy.
     *
     * @param int    $mockAMethodCountExpectation
     * @param int    $mockBMethodCountExpectation
     * @param int    $mockCMethodCountExpectation
     * @param bool   $mockAMethodResultExpectation
     * @param bool   $mockBMethodResultExpectation
     * @param bool   $mockCMethodResultExpectation
     * @param string $strategy
     * @param bool   $expectedCheckResult
     * @dataProvider checkingOrderAndEagerReturnDependingOnStrategyDataProvider
     */
    public function testCheckingOrderAndEagerReturnDependingOnStrategy(
        int $mockAMethodCountExpectation,
        int $mockBMethodCountExpectation,
        int $mockCMethodCountExpectation,
        bool $mockAMethodResultExpectation,
        bool $mockBMethodResultExpectation,
        bool $mockCMethodResultExpectation,
        string $strategy,
        bool $expectedCheckResult
    ): void {
        $checkerData = $this->getMockForAbstractClass(DataObjectInterface::class);

        /** @var CheckerInterface|\PHPUnit\Framework\MockObject\MockObject $checkerMockA */
        $checkerMockA = $this->getMockForAbstractClass(CheckerInterface::class);
        $checkerMockA->expects(static::exactly($mockAMethodCountExpectation))
            ->method('check')
            ->with($checkerData)
            ->willReturn($mockAMethodResultExpectation)
        ;

        /** @var CheckerInterface|\PHPUnit\Framework\MockObject\MockObject $checkerMockB */
        $checkerMockB = $this->getMockForAbstractClass(CheckerInterface::class);
        $checkerMockB->expects(static::exactly($mockBMethodCountExpectation))
            ->method('check')
            ->with($checkerData)
            ->willReturn($mockBMethodResultExpectation)
        ;

        /** @var CheckerInterface|\PHPUnit\Framework\MockObject\MockObject $checkerMockC */
        $checkerMockC = $this->getMockForAbstractClass(CheckerInterface::class);
        $checkerMockC->expects(static::exactly($mockCMethodCountExpectation))
            ->method('check')
            ->with($checkerData)
            ->willReturn($mockCMethodResultExpectation)
        ;

        $checkers = [
            $checkerMockA,
            $checkerMockB,
            $checkerMockC,
        ];

        $checker = new CheckerComposite($checkers, $strategy);
        static::assertSame($expectedCheckResult, $checker->check($checkerData));
    }

    /**
     * @return array
     */
    public function checkingOrderAndEagerReturnDependingOnStrategyDataProvider(): array
    {
        return [
            [1, 1, 1, true, true, true, CheckerComposite::STRATEGY_ALL, true],
            [1, 1, 1, true, true, false, CheckerComposite::STRATEGY_ALL, false],
            [1, 1, 0, true, false, true, CheckerComposite::STRATEGY_ALL, false],
            [1, 0, 0, false, true, true, CheckerComposite::STRATEGY_ALL, false],

            [1, 0, 0, true, true, true, CheckerComposite::STRATEGY_ANY, true],
            [1, 1, 0, false, true, true, CheckerComposite::STRATEGY_ANY, true],
            [1, 1, 1, false, false, true, CheckerComposite::STRATEGY_ANY, true],
            [1, 1, 1, false, false, false, CheckerComposite::STRATEGY_ANY, false],
        ];
    }

    /**
     * Check invalid checkers.
     *
     * @param array  $checkers
     * @param string $expectedExceptionMessage
     * @dataProvider invalidCheckerDefinitionsDataProvider
     */
    public function testInvalidCheckerDefinitions(
        array $checkers,
        string $expectedExceptionMessage
    ): void {
        $checkerData = $this->getMockForAbstractClass(DataObjectInterface::class);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $checker = new CheckerComposite($checkers);
        $checker->check($checkerData);
    }

    /**
     * Invalid checker definitions data provider.
     *
     * @return array
     */
    public function invalidCheckerDefinitionsDataProvider(): array
    {
        return [
            [
                [
                    'a string field',
                ],
                CheckerInterface::class . ' expected, string given',
            ],
            [
                [
                    new \stdClass(),
                ],
                CheckerInterface::class . ' expected, stdClass object given',
            ],
        ];
    }
}
