<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\CheckerComposite;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class CheckerCompositeFactory.
 *
 * @api
 */
class CheckerCompositeFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): CheckerComposite
    {
        $checkers = $data['checkers'] ?? [];
        $strategy = $data['strategy'] ?? null;
        $defaultCheckResult = $data['defaultCheckResult'] ?? null;

        return new CheckerComposite($checkers, $strategy, $defaultCheckResult);
    }
}
