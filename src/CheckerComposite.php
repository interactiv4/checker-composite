<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\CheckerComposite;

use Interactiv4\Contracts\Checker\Api\CheckerInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use RuntimeException;

/**
 * Class CheckerComposite.
 *
 * @api
 */
class CheckerComposite implements CheckerInterface
{
    /**
     * Composite check strategy:
     * - Return true if ALL checkers return true
     * - Return false if ANY checkers return false.
     *
     * See also $defaultCheckResult
     */
    const STRATEGY_ALL = 'all';

    /**
     * Composite check strategy:
     * - Return true if ANY checkers return true
     * - Return false if ALL checkers return false.
     *
     * See also $defaultCheckResult
     */
    const STRATEGY_ANY = 'any';

    /**
     * @var CheckerInterface[]
     */
    private $checkers;

    /**
     * @var string
     */
    private $strategy;

    /**
     * @var bool
     */
    private $defaultCheckResult;

    /**
     * CheckerComposite constructor.
     *
     * @param CheckerInterface[] $checkers
     * @param string|null        $strategy
     * @param bool               $defaultCheckResult
     */
    public function __construct(
        array $checkers = [],
        string $strategy = null,
        bool $defaultCheckResult = null
    ) {
        $this->checkers = $checkers;
        $this->strategy = $strategy ?? self::STRATEGY_ALL;
        $this->defaultCheckResult = $defaultCheckResult ?? true;
    }

    /**
     * Composite checker.
     *
     * {@inheritdoc}
     */
    public function check(DataObjectInterface $data = null): bool
    {
        $this->checkCheckers();

        foreach ($this->checkers as $checker) {
            $checkResult = $checker->check($data);

            // Check false result for STRATEGY_ALL
            if (false === $checkResult && self::STRATEGY_ALL === $this->strategy) {
                return $checkResult;
            }

            // Check true result for STRATEGY_ANY
            if (true === $checkResult && self::STRATEGY_ANY === $this->strategy) {
                return $checkResult;
            }
        }

        // At this point, last checkResult (if exists) is the proper check result
        return $checkResult ?? $this->defaultCheckResult;
    }

    /**
     * Check checkers definition.
     *
     * @throws RuntimeException
     */
    private function checkCheckers(): void
    {
        foreach ($this->checkers as $checker) {
            if (!\is_object($checker)) {
                $errorMessage = CheckerInterface::class . ' expected,' . ' ' . \gettype($checker) . ' given';
                throw new RuntimeException($errorMessage);
            }

            if (!$checker instanceof CheckerInterface) {
                $errorMessage = CheckerInterface::class . ' expected,' . ' ' . \get_class($checker) . ' object given';
                throw new RuntimeException($errorMessage);
            }
        }
    }
}
